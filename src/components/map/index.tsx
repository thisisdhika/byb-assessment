import * as React from 'react'
import clsx from 'clsx'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import { Spin, Typography } from 'antd'
import { PlacesAction } from '@/store/actions'
import { bindActionCreators, Dispatch } from 'redux'
import { AutoComplete, OptionType } from '@/components'
import { GoogleMap, LoadScript, Marker } from '@react-google-maps/api'

import './style.scss'

interface MapProps {
  defaultZoom?: number
  saveSelectedPlace?: (o: OptionType) => Promise<google.maps.GeocoderResponse>
}

const API_KEY = 'AIzaSyD7BlVUpYmxOLT1WtfVmZI0ZACkgG_r3ak'

const Map: React.FC<React.PropsWithChildren<MapProps>> = ({ defaultZoom, saveSelectedPlace }) => {
  const [zoom, setZoom] = React.useState(defaultZoom)
  const [showMarker, setShowMarker] = React.useState(false)
  const [center, setCenter] = React.useState<google.maps.LatLng | google.maps.LatLngLiteral>({ lat: -4.2838971, lng: 118.2612384 })
  const [markerPosition, setMarkerPosition] = React.useState<google.maps.LatLng | google.maps.LatLngLiteral>()

  const mapOptions = React.useMemo<google.maps.MapOptions>(
    () => ({
      zoomControl: false,
      mapTypeControl: false,
      streetViewControl: false,
      fullscreenControl: false,
      crossOriginIsolated: false,
    }),
    []
  )

  const handleAutoCompleteSelected = React.useCallback(async (selected?: OptionType) => {
    if (!!selected && !!saveSelectedPlace) {
      try {
        const { results } = await saveSelectedPlace(selected as OptionType)

        setZoom(12)
        setCenter(results[0].geometry.location)
        setMarkerPosition(results[0].geometry.location)
        setShowMarker(true)
      } catch (error) {
        console.error(error)
      }
    }
  }, [])

  return (
    <LoadScript
      googleMapsApiKey={API_KEY}
      libraries={['places', 'geometry']}
      loadingElement={
        <div className="map-loading">
          <Spin />
        </div>
      }
    >
      <GoogleMap mapContainerClassName="map" center={center} zoom={zoom} options={mapOptions}>
        <div className={clsx('map__overlay', showMarker && '-hide')} />
        <div className={clsx('map__autocomplete-wrapper', showMarker && '-in')}>
          <Typography.Title className="map__autocomplete-wrapper__ttl">Find Place!</Typography.Title>
          <Typography.Paragraph className="map__autocomplete-wrapper__desc">Type in the search box to find beautiful places.</Typography.Paragraph>
          <AutoComplete apiKey={API_KEY} onSelected={handleAutoCompleteSelected} />
        </div>
        {!!showMarker && <Marker position={markerPosition as typeof center} />}
      </GoogleMap>
    </LoadScript>
  )
}

Map.defaultProps = {
  defaultZoom: 5,
} as MapProps

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      saveSelectedPlace: PlacesAction.saveSelectedPlace,
    },
    dispatch
  )

export default compose<MapProps, MapProps>(connect(() => ({}), mapDispatchToProps))(Map)
