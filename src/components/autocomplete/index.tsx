import * as React from 'react'
import axios from 'axios'
import { compose } from 'recompose'
import debounce from 'lodash.debounce'
import { AutoComplete as AC, Input } from 'antd'

import './style.scss'
import { connect } from 'react-redux'

export type Option = {
  value: string
  place_id: string
  description: string
}

interface AutoCompleteProps {
  places?: any
  apiKey: string
  onSelected: (selected?: Option) => void
}

const AutoComplete: React.FC<AutoCompleteProps> = React.memo(({ apiKey, places, onSelected }) => {
  const [value, setValue] = React.useState('')
  const [options, setOptions] = React.useState<Option[]>([])

  const suggestions = React.useMemo(() => {
    const searchResult = options.map((prediction: any) => ({
      ...prediction,
      value: prediction.place_id,
      label: prediction.description,
    }))

    const lastPlacesOptions = places.lastPlaces.map((prediction: any) => ({
      ...prediction,
      value: prediction.place_id.concat('#keyed'),
      label: prediction.description,
    }))

    return [
      ...(searchResult.length > 0
        ? [
            {
              options: searchResult,
              label: <span>Search Result</span>,
            },
          ]
        : []),
      ...(lastPlacesOptions.length > 0
        ? [
            {
              options: lastPlacesOptions,
              label: <span>Recently Searched</span>,
            },
          ]
        : []),
    ]
  }, [options, places.lastPlaces])

  const searchPlaces = React.useCallback(
    async (keyword: string) =>
      await axios.get(`https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api/place/autocomplete/json`, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        data: {},
        params: {
          key: apiKey,
          input: keyword,
          types: 'geocode',
        },
      }),
    [apiKey]
  )

  const onSearch = debounce(
    React.useCallback(async (keyword: string) => {
      if (!!keyword) {
        try {
          const { data } = await searchPlaces(keyword)
          setOptions(data?.predictions ?? [])
        } catch (error) {
          console.error(error)
        }
      } else setOptions([])
    }, []),
    500
  )

  const onSelect = React.useCallback(
    (place_id: string) => {
      const selectedOption: Option | undefined = options.find((option: Option) => option.place_id === place_id)
      setValue(selectedOption?.description ?? '')
      onSelected(selectedOption)
    },
    [options]
  )

  return (
    <div className="autocomplete">
      <AC
        value={value}
        onChange={setValue}
        onSearch={onSearch}
        onSelect={onSelect}
        options={suggestions}
        dropdownMatchSelectWidth={480}
        className="autocomplete__wrapper"
        popupClassName="autocomplete__dropdown"
      >
        <Input.Search size="large" placeholder="Search..." />
      </AC>
    </div>
  )
})

const mapStateToProps = ({ places }: any) => ({
  places,
})

export default compose<AutoCompleteProps, AutoCompleteProps>(connect(mapStateToProps))(AutoComplete)
