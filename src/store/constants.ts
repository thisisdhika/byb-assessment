const makeCommonTypes = (type: string): Record<string, string> => ({
  BEGIN: type.concat('_BEGIN'),
  SUCCESS: type.concat('_SUCCESS'),
  FAILURE: type.concat('_FAILURE'),
})

export const SAVE_SELECTED_PLACE = makeCommonTypes('SAVE_SELECTED_PLACE')
