import { OptionType } from '@/components'
import { Action } from 'redux'
import { SAVE_SELECTED_PLACE } from '../constants'

const initialState: {
  error: any
  lastPlaces: OptionType[]
  isFetching: boolean
} = {
  error: null,
  lastPlaces: [],
  isFetching: false,
}

const placesReducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case SAVE_SELECTED_PLACE.BEGIN: {
      return {
        ...state,
        isFetching: true,
      }
    }

    case SAVE_SELECTED_PLACE.SUCCESS: {
      return {
        ...state,
        isFetching: false,
        lastPlaces: [...state.lastPlaces, (action as any).option],
      }
    }

    case SAVE_SELECTED_PLACE.FAILURE: {
      return {
        ...state,
        isFetching: false,
        error: (action as any).error,
      }
    }

    default:
      return state
  }
}

export default placesReducer
