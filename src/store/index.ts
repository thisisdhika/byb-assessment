import thunkMiddleware from 'redux-thunk'
import storage from 'redux-persist/lib/storage'
import { configureStore as configure } from '@reduxjs/toolkit'
import { persistStore, persistReducer } from 'redux-persist'

import rootReducers from '@/store/reducers'

// Types
import type { EnhancedStore } from '@reduxjs/toolkit'
import type { Persistor, PersistConfig } from 'redux-persist'

export type ConfiguredStore = {
  store: EnhancedStore
  persistor: Persistor
}

const configureStore = (): ConfiguredStore => {
  const initialState: Record<string, any> = {}

  const persistConfig: PersistConfig<typeof initialState> = {
    storage,
    key: `@byb-gautocomplete`,
    blacklist: [],
  }

  const persistedReducer = persistReducer(persistConfig, rootReducers as any)
  const store = configure({
    reducer: persistedReducer,
    preloadedState: initialState,
    middleware: [thunkMiddleware],
    devTools: !import.meta.env.PROD,
  })

  const persistor = persistStore(store)

  return {
    store,
    persistor,
  }
}

export default configureStore
