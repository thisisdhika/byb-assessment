import { Dispatch } from 'redux'
import { OptionType } from '@/components'
import { SAVE_SELECTED_PLACE } from '../constants'

const saveSelectedPlaceBegin = () => ({
  type: SAVE_SELECTED_PLACE.BEGIN,
})

const saveSelectedPlaceSuccess = (option: OptionType) => ({
  option,
  type: SAVE_SELECTED_PLACE.SUCCESS,
})

const saveSelectedPlaceFailure = (error: any) => ({
  error,
  type: SAVE_SELECTED_PLACE.FAILURE,
})

export const saveSelectedPlace =
  (option: OptionType): ((d: Dispatch) => Promise<google.maps.GeocoderResponse>) =>
  async (dispatch: Dispatch<any>): Promise<google.maps.GeocoderResponse> => {
    dispatch(saveSelectedPlaceBegin())

    try {
      const geocoder = new google.maps.Geocoder()
      const response = await geocoder.geocode({ placeId: option.place_id })

      dispatch(saveSelectedPlaceSuccess(option))

      return Promise.resolve<google.maps.GeocoderResponse>(response)
    } catch (error) {
      dispatch(saveSelectedPlaceFailure(error))

      return Promise.reject(error)
    }
  }
