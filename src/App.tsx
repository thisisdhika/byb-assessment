import * as React from 'react'
import { Map } from '@/components'
import configureStore from '@/store'
import { ConfigProvider } from 'antd'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import type { ThemeConfig } from 'antd/es/config-provider/context'

import '@/assets/sass/main.scss'

const { store, persistor } = configureStore()
const theme: ThemeConfig = {
  token: {
    //
  },
}

const App: React.FC = () => (
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <ConfigProvider theme={theme}>
        <Map />
      </ConfigProvider>
    </PersistGate>
  </Provider>
)

export default App
